#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <memory>

namespace {
	using my_type = double;
	using integer_type = int;
	
	// const integer_type Nx = 8'000;
	// const integer_type Ny = 8'000;
	// const integer_type Tn = 120;
	const integer_type Nx = 100;
	const integer_type Ny = 100;
	const integer_type Tn = 5000;

	const my_type Xa = 0.0;
	const my_type Xb = 4.0;
	const my_type Ya = 0.0;
	const my_type Yb = 4.0;

	const my_type hx = (Xb - Xa) / (Nx - 1);
	const my_type hy = (Yb - Ya) / (Ny - 1);

	const integer_type one_grid = Nx * Ny;

	const char* out_file_name = "out.txt";

	union ticks {
		std::uint64_t t64;
		struct s32 { std::uint32_t th, tl; } t32;
	} start, end;
	const double cpu_Hz = 2700000000ULL;

} // namespace

//declaration
void init_ro(my_type* ro);
my_type* calculate_F(my_type* F1, my_type* F2, my_type* ro, integer_type layer);

static inline void record_time_start() {
	asm("rdtsc\n":"=a"(start.t32.th),"=d"(start.t32.tl));
}

static inline void record_time_end() {
	asm("rdtsc\n":"=a"(end.t32.th),"=d"(end.t32.tl));
}

void hot_f() {
	volatile double k = 10;
	for (std::uint64_t i = 0; i < 150'000'000; ++i) {
		k = std::sin(k);
	}
}

int main() {

	std::unique_ptr<my_type[]> ro_ptr = std::make_unique<my_type[]>(Nx * Ny);
	my_type* ro = ro_ptr.get();
	init_ro(ro);

	std::unique_ptr<my_type[]> F1_ptr = std::make_unique<my_type[]>(one_grid); 
	std::unique_ptr<my_type[]> F2_ptr = std::make_unique<my_type[]>(one_grid); 
	my_type* F1 = F1_ptr.get();
	my_type* F2 = F2_ptr.get();
	for (integer_type i = 0; i < one_grid; ++i) {
		F1[i] = 0;
	}

	auto F = calculate_F(F1, F2, ro, 8);

	// std::ofstream file(out_file_name);
	// file.write(reinterpret_cast<char*>(F), sizeof(my_type) * one_grid);

	return 0;
}

inline void init_ro(my_type* ro) {
	const my_type one_third = 1.0 / 3.0;
	const my_type dif_X = Xb - Xa;
	const my_type dif_Y = Yb - Ya;
	const my_type Xs1 = Xa + dif_X * one_third;
	const my_type Ys1 = Ya + dif_Y * (2 * one_third);
	const my_type Xs2 = Xa + dif_X * (2 * one_third);
	const my_type Ys2 = Ya + dif_Y * one_third;
	const my_type sqr_R = std::pow(0.1 * std::min(dif_X, dif_Y), 2);

	my_type cur_x_arr[Nx];
	my_type cur_tmp = 0;
	for (std::size_t i = 0; i < Nx; ++i) {
		cur_x_arr[i] = cur_tmp;
		cur_tmp += hx;
	}

	std::size_t cur_y_position = 0;
	my_type cur_y = 0;
	for (std::size_t y = 0; y < Ny; ++y) {
		my_type cur_dif_y1 = std::pow(cur_y - Ys1, 2);
		my_type cur_dif_y2 = std::pow(cur_y - Ys2, 2);
		for (std::size_t x = 0; x < Nx; ++x) {
			if (sqr_R > cur_dif_y1 + std::pow(cur_x_arr[x] - Xs1, 2)) {
				ro[cur_y_position + x] = 0.1;
			} else {
				if (sqr_R > cur_dif_y2 + std::pow(cur_x_arr[x] - Xs2, 2)) {
					ro[cur_y_position + x] = -0.1;
				} else {
					ro[cur_y_position + x] = 0.0;
				}
			}
		}
		cur_y += hy;
		cur_y_position += Nx;
	}
}

my_type calculate_error(my_type* F1, my_type* F2);

inline my_type* calculate_F(my_type* F1, my_type* F2, my_type* ro, integer_type layer) {
	const my_type one_dev_sqr_hx = 1 / (hx * hx);
	const my_type one_dev_sqr_hy = 1 / (hy * hy);

	const my_type first_multiplier = 0.2 / (one_dev_sqr_hy + one_dev_sqr_hx);
	const my_type second_multiplier = 0.5 * (one_dev_sqr_hx * 5 - one_dev_sqr_hy);
	const my_type third_multiplier = 0.5 * (one_dev_sqr_hy * 5 - one_dev_sqr_hx);
	const my_type fourth_multiplier = 0.25 * (one_dev_sqr_hx + one_dev_sqr_hy);

	my_type* input_F1 = F1;
	my_type* input_F2 = F2;
	hot_f();
	record_time_start(); //time
	integer_type i = 0;
	integer_type end_c = Nx - 1;
	for (integer_type n = 0; n < Tn; n += layer) {
		integer_type before_cur_y = 0;
		integer_type cur_y = Nx;
		integer_type after_cur_y = 2 * Nx;
		
		//init
		{
			for (integer_type k = layer; k != 0; --k) {
				for (integer_type l = 0; l < layer; ++l) {
					for (integer_type j = 1; j < end_c; ++j) {
						F2[cur_y + j] = 
							first_multiplier * (
								second_multiplier * (F1[cur_y + j - 1] + F1[cur_y + j + 1])
								+ third_multiplier * (F1[before_cur_y + j] + F1[after_cur_y + j])
								+ fourth_multiplier * (F1[before_cur_y + j - 1]
														+ F1[before_cur_y + j + 1]
														+ F1[after_cur_y + j - 1]
														+ F1[after_cur_y + j + 1])
								+ 2 * ro[cur_y + j]
								+ 0.25 * (ro[before_cur_y + j] + ro[after_cur_y + j] + ro[cur_y + j - 1] + ro[cur_y + j + 1])
							);
					}
				}
				std::swap(F1, F2);
				before_cur_y = cur_y;
				cur_y = after_cur_y;
				after_cur_y += Nx;
			}

		}

		F1 = input_F1;
		F2 = input_F2;

		//main calc
		for (i = layer + 1; i < end_c; ++i) {
			cur_y = i * Nx;
			before_cur_y = cur_y - Nx;
			after_cur_y = cur_y + Nx;
			for (integer_type h = 0; h < layer; ++h) {
				for (integer_type j = 1; j < end_c; ++j) {
					F2[cur_y + j] = 
						first_multiplier * (
							second_multiplier * (F1[cur_y + j - 1] + F1[cur_y + j + 1])
							+ third_multiplier * (F1[before_cur_y + j] + F1[after_cur_y + j])
							+ fourth_multiplier * (F1[before_cur_y + j - 1]
													+ F1[before_cur_y + j + 1]
													+ F1[after_cur_y + j - 1]
													+ F1[after_cur_y + j + 1])
							+ 2 * ro[cur_y + j]
							+ 0.25 * (ro[before_cur_y + j] + ro[after_cur_y + j] + ro[cur_y + j - 1] + ro[cur_y + j + 1])
						);
				}
				after_cur_y = cur_y;
				cur_y = before_cur_y;
				before_cur_y -= Nx;
				std::swap(F1, F2);
			}
			F1 = input_F1;
			F2 = input_F2;
		}

		std::swap(F1, F2);

		// end
		for (integer_type h = 1; h < layer; ++h) {
			for (i = Nx - h; i < Nx; ++i) {
				for (integer_type j = 1; j < end_c; ++j) {
					F2[cur_y + j] = 
						first_multiplier * (
							second_multiplier * (F1[cur_y + j - 1] + F1[cur_y + j + 1])
							+ third_multiplier * (F1[before_cur_y + j] + F1[after_cur_y + j])
							+ fourth_multiplier * (F1[before_cur_y + j - 1]
													+ F1[before_cur_y + j + 1]
													+ F1[after_cur_y + j - 1]
													+ F1[after_cur_y + j + 1])
							+ 2 * ro[cur_y + j]
							+ 0.25 * (ro[before_cur_y + j] + ro[after_cur_y + j] + ro[cur_y + j - 1] + ro[cur_y + j + 1])
						);
				}
			}
			std::swap(F1, F2);
		}
		std::cout << "Err = " << calculate_error(F1, F2) << std::endl;
	}

	if (!(Tn % layer)) {
		for (integer_type n = 0, local_end = Tn % layer; n < local_end; ++n) {

			integer_type before_cur_y = 0;
			integer_type cur_y = Nx;
			integer_type after_cur_y = 2 * Nx;
			for (integer_type i = 1, end_i = Ny - 1; i < end_i; ++i) {
				for (integer_type j = 1, end_j = Nx - 1; j < end_j; ++j) {
					F2[cur_y + j] = 
						first_multiplier * (
							second_multiplier * (F1[cur_y + j - 1] + F1[cur_y + j + 1])
							+ third_multiplier * (F1[before_cur_y + j] + F1[after_cur_y + j])
							+ fourth_multiplier * (F1[before_cur_y + j - 1]
													+ F1[before_cur_y + j + 1]
													+ F1[after_cur_y + j - 1]
													+ F1[after_cur_y + j + 1])
							+ 2 * ro[cur_y + j]
							+ 0.25 * (ro[before_cur_y + j] + ro[after_cur_y + j] + ro[cur_y + j - 1] + ro[cur_y + j + 1])
						);
				}
				before_cur_y = cur_y;
				cur_y = after_cur_y;
				after_cur_y += Nx;
			}

			std::swap(F1, F2);
		}

	}
	record_time_end();
	// printf("Time taken: %lf sec.\n", (end.t64-start.t64)/cpu_Hz);
	return F2;
}

my_type calculate_error(my_type* F1, my_type* F2) {
	my_type my_error = 0.0;
	my_type new_value = 0.0;
	for (integer_type i = 0; i < Nx; ++i) {
		for (integer_type j = 0; j < Ny; ++j) {
			new_value = std::abs(F1[Nx * i + j] - F2[Nx * i + j]);
			if (new_value > my_error) {
				my_error = new_value;
			}
		}
	}
	return my_error;
}