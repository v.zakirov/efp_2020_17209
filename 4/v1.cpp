//g++ v1.cpp -o v1 -Wall -std=c++11 -O3 -march=native -lpthread -fopenmp
#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <memory>
#include <cstring>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <thread>
#include <functional>
#include <omp.h>

namespace {
	using my_type = double;
	using integer_type = int;
	
	// const integer_type Nx = 8000;
	// const integer_type Ny = 8000;
	// const integer_type Tn = 120;
	const integer_type Nx = 100;
	const integer_type Ny = 100;
	const integer_type Tn = 5000;

	const my_type Xa = 0.0;
	const my_type Xb = 4.0;
	const my_type Ya = 0.0;
	const my_type Yb = 4.0;

	const my_type hx = (Xb - Xa) / (Nx - 1);
	const my_type hy = (Yb - Ya) / (Ny - 1);

	const integer_type one_grid = Nx * Ny;

	const char* out_file_name = "out.txt";


	class Barrier {
	public:
		Barrier(int count) : thread_count(count), counter(0), waiting(0) {}

		void wait() {
			std::unique_lock<std::mutex> lk(m);
			++counter;
			++waiting;
			cv.wait(lk, [&] { return counter >= thread_count; });
			cv.notify_all();
			--waiting;
			if (waiting == 0) {
				counter = 0;
			}
		}

	private:
		std::mutex m;
		std::condition_variable cv;
		int thread_count;
		int counter;
		int waiting;
	};

} // namespace

//declaration
void init_ro(my_type* ro);
void calculate_F(int number
	, integer_type start_y
	, integer_type end_y
	, my_type* F1, my_type* F2
	, my_type* ro
	, Barrier& my_b_one
	, Barrier& my_b_two);

void hot_f() {
	volatile double k = 10;
	for (std::uint64_t i = 0; i < 150000000; ++i) {
		k = std::sin(k);
	}
}

int main(int argc, char const *argv[]) {
	if (2 != argc) {
		std::cout << "argument error" << std::endl;
		return 0;
	}
	int num_threads = atoi(argv[1]);
	std::vector<std::thread> threads;
	threads.reserve(num_threads);
	std::vector<double> res;
	res.resize(num_threads, 0.0);

	//alloc
	double* ro = (double*)_mm_malloc(one_grid * sizeof(double), 32);
	init_ro(ro);

	double* F1 = (double*)_mm_malloc(one_grid * 2 * sizeof(double), 32);
	double* F2 = F1 + one_grid;
	std::memset(F1, 0, one_grid * 2 * sizeof(my_type));


	Barrier one(num_threads);
	Barrier two(num_threads);

	//calc
	integer_type step((Ny - 2) / num_threads);

	integer_type remains((Ny - 2) % num_threads);

	integer_type end_first = 1 + step + (remains == 0 ? 0 : 1);

	if (remains > 0) {
		-- remains;
	}
	integer_type start_y(end_first);
	integer_type end_y(0);


	double start_time = omp_get_wtime();
	for (int i = 1; i < num_threads; ++i) {
		end_y = start_y + step + (remains == 0 ? 0 : 1);
		if (remains != 0) {
			--remains;
		}
		threads[i] = std::thread(calculate_F, i, start_y, end_y, F1, F2, ro, std::ref(one), std::ref(two));
		start_y = end_y;
	}

	calculate_F(0, 1, end_first, F1, F2, ro, std::ref(one), std::ref(two));
	for (int i = 1; i < num_threads; ++i) {
		threads[i].join();
	}
	double end_time = omp_get_wtime();;


	// std::ofstream file(out_file_name);
	// file.write(reinterpret_cast<char*>(F2), sizeof(my_type) * one_grid);

	std::cout << "threads := " << num_threads << "  total time := " << end_time - start_time << " second" << std::endl;

	_mm_free(ro);
	_mm_free(F1);

	return 0;
}

my_type calculate_error(my_type* F1, my_type* F2);

void calculate_F(int number
	, integer_type start_y
	, integer_type end_y
	, my_type* F1
	, my_type* F2
	, my_type* ro
	, Barrier& my_b_one
	, Barrier& my_b_two
	) {


	const my_type one_dev_sqr_hx = 1 / (hx * hx);
	const my_type one_dev_sqr_hy = 1 / (hy * hy);

	const my_type first_multiplier = 0.2 / (one_dev_sqr_hy + one_dev_sqr_hx);
	const my_type second_multiplier = 0.5 * (one_dev_sqr_hx * 5 - one_dev_sqr_hy);
	const my_type third_multiplier = 0.5 * (one_dev_sqr_hy * 5 - one_dev_sqr_hx);
	const my_type fourth_multiplier = 0.25 * (one_dev_sqr_hx + one_dev_sqr_hy);

	for (integer_type n = 0; n < Tn; ++n) {
		integer_type before_cur_y = Nx * (start_y - 1);
		integer_type cur_y = Nx * start_y;
		integer_type after_cur_y = Nx * (start_y + 1);
		for (integer_type i = start_y; i < end_y; ++i) {
			for (integer_type j = 1; j < Nx - 1; ++j) {
				F2[cur_y + j] = 
					first_multiplier * (
						second_multiplier * (F1[cur_y + j - 1] + F1[cur_y + j + 1])
						+ third_multiplier * (F1[before_cur_y + j] + F1[after_cur_y + j])
						+ fourth_multiplier * (F1[before_cur_y + j - 1]
												+ F1[before_cur_y + j + 1]
												+ F1[after_cur_y + j - 1]
												+ F1[after_cur_y + j + 1])
						+ 2 * ro[cur_y + j]
						+ 0.25 * (ro[before_cur_y + j] + ro[after_cur_y + j] + ro[cur_y + j - 1] + ro[cur_y + j + 1])
					);
			}
			before_cur_y = cur_y;
			cur_y = after_cur_y;
			after_cur_y += Nx;
		}

		if (1 & n) {
			my_b_one.wait();
		} else {
			my_b_two.wait();
		}
		std::swap(F1, F2);
	}
}

my_type calculate_error(my_type* F1, my_type* F2) {
	my_type my_error = 0.0;
	my_type new_value = 0.0;
	for (integer_type i = 0; i < Nx; ++i) {
		for (integer_type j = 0; j < Ny; ++j) {
			new_value = std::abs(F1[Nx * i + j] - F2[Nx * i + j]);
			if (new_value > my_error) {
				my_error = new_value;
			}
		}
	}
	return my_error;
}

inline void init_ro(my_type* ro) {
	const my_type one_third = 1.0 / 3.0;
	const my_type dif_X = Xb - Xa;
	const my_type dif_Y = Yb - Ya;
	const my_type Xs1 = Xa + dif_X * one_third;
	const my_type Ys1 = Ya + dif_Y * (2 * one_third);
	const my_type Xs2 = Xa + dif_X * (2 * one_third);
	const my_type Ys2 = Ya + dif_Y * one_third;
	const my_type sqr_R = std::pow(0.1 * std::min(dif_X, dif_Y), 2);

	my_type cur_x_arr[Nx];
	my_type cur_tmp = 0;
	for (std::size_t i = 0; i < Nx; ++i) {
		cur_x_arr[i] = cur_tmp;
		cur_tmp += hx;
	}

	std::size_t cur_y_position = 0;
	my_type cur_y = 0;
	for (std::size_t y = 0; y < Ny; ++y) {
		my_type cur_dif_y1 = std::pow(cur_y - Ys1, 2);
		my_type cur_dif_y2 = std::pow(cur_y - Ys2, 2);
		for (std::size_t x = 0; x < Nx; ++x) {
			if (sqr_R > cur_dif_y1 + std::pow(cur_x_arr[x] - Xs1, 2)) {
				ro[cur_y_position + x] = 0.1;
			} else {
				if (sqr_R > cur_dif_y2 + std::pow(cur_x_arr[x] - Xs2, 2)) {
					ro[cur_y_position + x] = -0.1;
				} else {
					ro[cur_y_position + x] = 0.0;
				}
			}
		}
		cur_y += hy;
		cur_y_position += Nx;
	}
}