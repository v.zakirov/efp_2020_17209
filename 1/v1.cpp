#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <memory>

namespace {
	// const std::size_t Nx = 8'000;
	// const std::size_t Ny = 8'000;
	// const std::size_t Tn = 100;
	const std::size_t Nx = 100;
	const std::size_t Ny = 100;
	const std::size_t Tn = 5000;

	using my_type = double;

	const my_type Xa = 0.0;
	const my_type Xb = 4.0;
	const my_type Ya = 0.0;
	const my_type Yb = 4.0;

	const my_type hx = (Xb - Xa) / (Nx - 1);
	const my_type hy = (Yb - Ya) / (Ny - 1);

	const std::size_t one_grid = Nx * Ny;

	const char* out_file_name = "out.txt";

	union ticks{
		std::uint64_t t64;
		struct s32 { std::uint32_t th, tl; } t32;
	} start, end;
	double cpu_Hz = 2700000000ULL;

} // namespace

void init_ro(my_type* ro);
my_type* calculate_F(my_type* F1, my_type* F2, my_type* ro);

static inline void record_time_start() {
	asm("rdtsc\n":"=a"(start.t32.th),"=d"(start.t32.tl));
}

static inline void record_time_end() {
	asm("rdtsc\n":"=a"(end.t32.th),"=d"(end.t32.tl));
}

int main() {

	std::unique_ptr<my_type[]> ro_ptr = std::make_unique<my_type[]>(Nx * Ny);
	my_type* ro = ro_ptr.get();
	init_ro(ro);

	std::unique_ptr<my_type[]> F1_ptr = std::make_unique<my_type[]>(one_grid); 
	std::unique_ptr<my_type[]> F2_ptr = std::make_unique<my_type[]>(one_grid); 
	my_type* F1 = F1_ptr.get();
	my_type* F2 = F2_ptr.get();
	for (std::size_t i = 0; i < one_grid; ++i) {
		F1[i] = 0;
	}


	my_type* F = nullptr;
	F = calculate_F(F1, F2, ro);

	std::ofstream file(out_file_name);

	// file.write(reinterpret_cast<char*>(F), sizeof(my_type) * one_grid);

	return 0;
}

inline void init_ro(my_type* ro) {
	const my_type one_third = 1.0 / 3.0;
	const my_type dif_X = Xb - Xa;
	const my_type dif_Y = Yb - Ya;
	const my_type Xs1 = Xa + dif_X * one_third;
	const my_type Ys1 = Ya + dif_Y * (2 * one_third);
	const my_type Xs2 = Xa + dif_X * (2 * one_third);
	const my_type Ys2 = Ya + dif_Y * one_third;
	const my_type sqr_R = std::pow(0.1 * std::min(dif_X, dif_Y), 2);

	my_type cur_x_arr[Nx];
	my_type cur_tmp = 0;
	for (std::size_t i = 0; i < Nx; ++i) {
		cur_x_arr[i] = cur_tmp;
		cur_tmp += hx;
	}

	std::size_t cur_y_position = 0;
	my_type cur_y = 0;
	for (std::size_t y = 0; y < Ny; ++y) {
		my_type cur_dif_y1 = std::pow(cur_y - Ys1, 2);
		my_type cur_dif_y2 = std::pow(cur_y - Ys2, 2);
		for (std::size_t x = 0; x < Nx; ++x) {
			if (sqr_R > cur_dif_y1 + std::pow(cur_x_arr[x] - Xs1, 2)) {
				ro[cur_y_position + x] = 0.1;
			} else {
				if (sqr_R > cur_dif_y2 + std::pow(cur_x_arr[x] - Xs2, 2)) {
					ro[cur_y_position + x] = -0.1;
				} else {
					ro[cur_y_position + x] = 0.0;
				}
			}
		}
		cur_y += hy;
		cur_y_position += Nx;
	}
}

inline my_type* calculate_F(my_type* F1, my_type* F2, my_type* ro) {
	const my_type one_dev_sqr_hx = 1 / (hx * hx);
	const my_type one_dev_sqr_hy = 1 / (hy * hy);
	const my_type one_dev_sum_h = 1 / (one_dev_sqr_hy + one_dev_sqr_hx);

	record_time_start();
	for (std::size_t n = 0; n < Tn; ++n) {
		for (std::size_t i = 1, end_i = Ny - 1; i < end_i; ++i) {
			for (std::size_t j = 1, end_j = Nx - 1; j < end_j; ++j) {
				F2[i * Nx + j] = 
					(one_dev_sum_h * 0.2) * (
						0.5 * (one_dev_sqr_hx * 5 - one_dev_sqr_hy) * (F1[i * Nx + j - 1] + F1[i * Nx + j + 1])
						+ 0.5 * (one_dev_sqr_hy * 5 - one_dev_sqr_hx) * (F1[(i - 1) * Nx + j] + F1[(i + 1) * Nx + j])
						+ 0.25 * (one_dev_sqr_hx + one_dev_sqr_hy) * (F1[(i - 1) * Nx + j - 1]
																		+ F1[(i - 1) * Nx + j + 1]
																		+ F1[(i + 1) * Nx + j - 1]
																		+ F1[(i + 1) * Nx + j + 1])
						+ 2 * ro[i * Nx + j]
						+ 0.25 * (ro[(i - 1) * Nx + j] + ro[(i + 1) * Nx + j] + ro[i * Nx + j - 1] + ro[i * Nx + j + 1])
					);
			}
		}
		
		std::swap(F1, F2);
	}
	record_time_end();
	printf("Time taken: %lf sec.\n", (end.t64-start.t64)/cpu_Hz);
	return F2;
}
